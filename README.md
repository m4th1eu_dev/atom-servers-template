# Atom Servers Templates

Hi! This pack contains multiples Atom servers templates.

> **Currently this pack contains :**<br/>
	- A bungeecord network.<br/>
	- A simple server.<br/>
	
More features coming soon.

I try to update the templates most often.<br/><br/>


##### Official Atom [Repository](https://gitlab.com/AtomMC/Atom)
##### Official Atom Bukkit [Repository](https://gitlab.com/AtomMC/Bukkit)