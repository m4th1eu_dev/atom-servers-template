@echo off

set title=Minecraft Atom Bungee Server
set jar=atom_minecraft_server.jar
set MINRAM=1G
set MAXRAM=4G

title %title%
cls
echo ========================
echo !!                    !!
echo !!  MINECRAFT ATOM    !!
echo !!      SERVER        !!
echo !!                    !!
echo ========================
echo.
echo.
echo Starting Server !
echo.
echo.
java -Xms%MINRAM% -Xmx%MAXRAM% -jar %jar% nogui
echo.
echo.
color 0c
echo Server Stopped.

pause>nul